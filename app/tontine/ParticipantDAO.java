package tontine;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


//import controllers.Persone;
//import controllers.Participant;

public class ParticipantDAO extends JDBC{
	
	
	public String ajouter(Participant p) throws ClassNotFoundException, SQLException {

		try {
			prepareStat = this.jdbc().prepareStatement("insert into  tontine.participant values (default, ?, ?, ?, ?, ?, ?) ");
			prepareStat.setString(1, p.getNom());
			prepareStat.setString(2, p.getPrenom());
			prepareStat.setInt(3, p.getTelephone());
			prepareStat.setString(4, p.getDateNaissance());
			prepareStat.setString(5, p.getGenre());
			prepareStat.setString(6, p.getRole());
			
			prepareStat.executeUpdate();
			return "ok";
		} catch (SQLException e) {
			return e.getMessage();
		}

	}
	
	 public List<Participant> listeP() throws ClassNotFoundException, SQLException {
			statement = this.jdbc().createStatement();
			List<Participant> listes = new ArrayList<Participant>();
			Participant c = new Participant();
		    result = statement.executeQuery("select * from tontine.participant");
		    
		    while (result.next()) {
		    	c.setNumP(Integer.valueOf(result.getString("numP")));
				c.setNom(result.getString("nom"));
				c.setPrenom(result.getString("prenom"));	
				c.setTelephone(Integer.valueOf(result.getString("telephone")));;
				c.setDateNaissance(result.getString("dateNaissance"));
				c.setGenre(result.getString("genre"));
				c.setRole(result.getString("role"));
				listes.add(c);
				c = new Participant();
			}
			return listes;
			
		}
	 public Participant findByID(int id) throws ClassNotFoundException, SQLException {

			statement = this.jdbc().createStatement();
			Participant c = new Participant();
			// les resultat de la requete sont stocker dans l'objet result
			prepareStat = this.jdbc().prepareStatement("select * from tontine.participant where numP =? ");
			prepareStat.setInt(1, id);
			result = prepareStat.executeQuery();

			if (result.next()) {

				c.setNumP(Integer.valueOf(result.getString("numP").replace(" ", "")));
				c.setNom(result.getString("nom"));
				c.setPrenom(result.getString("prenom"));	
				c.setTelephone(Integer.valueOf(result.getString("telephone")));;
				c.setDateNaissance(result.getString("dateNaissance"));
				c.setRole(result.getString("genre"));

			}

			return c;

		}
	 public String suppression(int id) throws SQLException, ClassNotFoundException {
		 String sql = "DELETE FROM tontine.participant WHERE numP=?";
		 
			prepareStat = this.jdbc().prepareStatement(sql);
			prepareStat.setInt(1, id);
			 
			int rowsDeleted = prepareStat.executeUpdate();
			if (rowsDeleted > 0) {
			    System.out.println("Participant deleted successfully!");
			    return "OK";
			}else
				return "erreur";
	 	}

		public String update(Participant p) throws SQLException {
			try {
				String sql = "UPDATE tontine.participant SET nom=?,prenom=?,telephone=?,dateNaissance=?,genre=?,role=?) WHERE numP=?";
				 
				prepareStat = this.jdbc().prepareStatement(sql);
				 prepareStat.setString(1, p.getNom());
				 prepareStat.setString(2 , p.getPrenom());
				 prepareStat.setInt(3, p.getTelephone());
				 prepareStat.setString(4, p.getDateNaissance());
				 prepareStat.setString(5, p.getGenre());
				 prepareStat.setString(5, p.getRole());
				
				int rowsUpdated = prepareStat.executeUpdate();
				
				if (rowsUpdated > 0) {
				    System.out.println("Participant was updated successfully!");
				}
				return "ok";
			} catch (Exception e) {
				return e.getMessage();
			}

		}
 }
