package tontine;


public class Tontine {
	private int numT;

	private String libelle;
	private int montantSingulier;
	
	private String dateDeb;
	private String dateFin;
	private int nbrParticipant;
	
	
	
	
	public int getNumT() {
		return numT;
	}
	public void setNumT(int numT) {
		this.numT = numT;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	public int getMontantSingulier() {
		return montantSingulier;
	}
	public void setMontantSingulier(int montantSingulier) {
		this.montantSingulier = montantSingulier;
	}
	public String getDateDeb() {
		return dateDeb;
	}
	public void setDateDeb(String dateDeb) {
		this.dateDeb = dateDeb;
	}
	public String getDateFin() {
		return dateFin;
	}
	public void setDateFin(String dateFin) {
		this.dateFin = dateFin;
	}
	public int getNbrParticipant() {
		return nbrParticipant;
	}
	public void setNbrParticipant(int nbrParticipant) {
		this.nbrParticipant = nbrParticipant;
	}
}
