package tontine;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TontineDAO extends JDBC {
	public String ajouter(Tontine t) throws ClassNotFoundException, SQLException {

		try {
			prepareStat = this.jdbc().prepareStatement("insert into  tontine.tontine values (default, ?, ?, ?, ?, ?) ");
			 prepareStat.setString(1, t.getLibelle());
			 prepareStat.setInt(2 , t.getMontantSingulier());
			 prepareStat.setString(3, t.getDateDeb());
			 prepareStat.setString(4, t.getDateFin()); 
			 prepareStat.setInt(5, t.getNbrParticipant());
			
			// prepareStat.setDate(4, new java.sql.Date(2009, 12, 11));
			prepareStat.executeUpdate();
			return "ok";
		} catch (SQLException e) {
			return e.getMessage();
		}

	}
	
	 public List<Tontine> listeT() throws ClassNotFoundException, SQLException {
			statement = this.jdbc().createStatement();
			List<Tontine> listes = new ArrayList<Tontine>();
			Tontine t = new Tontine();
		    result = statement.executeQuery("select * from tontine.tontine");
		    
		    while (result.next()) {
		    	t.setNumT(Integer.valueOf(result.getString("numT")));
				t.setLibelle(result.getString("libelle"));
				t.setMontantSingulier(Integer.valueOf(result.getString("montantSingulier")));	
				t.setDateDeb(result.getString("dateDeb"));;
				t.setDateFin(result.getString("dateFin"));
				t.setNbrParticipant(Integer.valueOf(result.getString("nbrParticipant")));
				listes.add(t);
				t = new Tontine();
			}
			return listes;
			
		}
	 public Tontine findByID(int id) throws ClassNotFoundException, SQLException {

			statement = this.jdbc().createStatement();
			Tontine t = new Tontine();
			// les resultat de la requete sont stocker dans l'objet result
			prepareStat = this.jdbc().prepareStatement("select * from tontine.tontine where numT=?");
			prepareStat.setInt(1, id);
			result = prepareStat.executeQuery();
			
			if (result.next()) {

				t.setNumT(Integer.valueOf(result.getString("numT").replace(" ", "")));
				t.setLibelle(result.getString("libelle"));
				t.setMontantSingulier(Integer.valueOf(result.getString("montantSingulier")));	
				t.setDateDeb(result.getString("dateDeb"));;
				t.setDateFin(result.getString("dateFin"));
				t.setNbrParticipant(Integer.valueOf(result.getInt("nbrParticipant")));
			}

			return t;

		}
	 public String suppression(int id) throws SQLException, ClassNotFoundException {
		 String sql = "DELETE FROM tontine.tontine WHERE numT=?";
		 
			prepareStat = this.jdbc().prepareStatement(sql);
			prepareStat.setInt(1, id);
			 
			int rowsDeleted = prepareStat.executeUpdate();
			if (rowsDeleted > 0) {
			    System.out.println("Tontine deleted successfully!");
			    return "OK";
			}else
				return "erreur";
	 	}

		public String update(Tontine t) throws SQLException {
			try {
				String sql = "UPDATE tontine.tontine SET libelle=?,mntantSingulier=?,dateDeb=?,dateFin=?,nbrParticipant=?) WHERE numT=?";
				 
				prepareStat = this.jdbc().prepareStatement(sql);
				 prepareStat.setString(1, t.getLibelle());
				 prepareStat.setInt(2 , t.getMontantSingulier());
				 prepareStat.setString(3, t.getDateDeb());
				 prepareStat.setString(4, t.getDateFin()); 
				 prepareStat.setInt(5, t.getNbrParticipant());
				
				
				int rowsUpdated = prepareStat.executeUpdate();
				
				if (rowsUpdated > 0) {
				    System.out.println("Tontine was updated successfully!");
				}
				return "ok";
			} catch (Exception e) {
				return e.getMessage();
			}

		}

}
