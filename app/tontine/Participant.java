package tontine;

public class Participant {

	private int numP;
	private String nom;
	private String prenom;
	private int telephone;
	private String dateNaissance;
	private String genre;
	private String role;
	

	public Participant() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getNumP() {
		return numP;
	}
	public void setNumP(int numP) {
		this.numP = numP;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public int getTelephone() {
		return telephone;
	}
	public void setTelephone(int telephone) {
		this.telephone = telephone;
	}
	public String getDateNaissance() {
		return dateNaissance;
	}
	public void setDateNaissance(String dateNaissance) {
		this.dateNaissance = dateNaissance;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
	
}
