package tontine;

public class Versement {

	private String date;
	private int montant;
	private int numP;
	private int numT;
	
	public int getNumP() {
		return numP;
	}
	public void setNumP(int numP) {
		this.numP = numP;
	}
	public int getNumT() {
		return numT;
	}
	public void setNumT(int numT) {
		this.numT = numT;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Number getMontant() {
		return montant;
	}
	public void setMontant(int montant) {
		this.montant = montant;
	}

	
	
}
