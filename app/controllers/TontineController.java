package controllers;

import java.sql.SQLException;
import javax.inject.Inject;
import play.data.Form;
import play.mvc.*;
import play.mvc.Http.Request;
import play.data.FormFactory;
import tontine.Participant;
import tontine.ParticipantDAO;
import tontine.Tontine;
import tontine.TontineDAO;

public class TontineController extends Controller {
	
	FormFactory dataForm;
	TontineDAO tontineDAO;
	@Inject
	public TontineController(FormFactory dataForm, TontineDAO tontineDAO) {
		super();
		this.dataForm = dataForm;
		this.tontineDAO = tontineDAO;
	}
	
	public Result createT(Request request){
		return ok(views.html.CreateT.render(request));
	}
	
	public Result saveT(Request request) throws ClassNotFoundException, SQLException {

		Form<Tontine> form = dataForm.form(Tontine.class).bindFromRequest(request);
		Tontine tontine = form.get();
		
		if(tontineDAO.ajouter(tontine).equals("ok"))
			System.out.println("insertion effectuer avec success");
		else
			System.err.println("insertion non effectuer" + tontineDAO.ajouter(tontine));
		
		return ok(views.html.AfficherTontine.render(tontineDAO.listeT()));

	}
	
	public Result listeT(Request request) throws ClassNotFoundException, SQLException{
		return ok(views.html.AfficherTontine.render(tontineDAO.listeT()));
	}

	public Result modifView(Request request, int id) throws ClassNotFoundException, SQLException{
		return ok(views.html.UpdateT.render(tontineDAO.findByID(id),request));
	}
	
	
	public Result updateT(Request request) throws ClassNotFoundException, SQLException{
		Form<Tontine> form = dataForm.form(Tontine.class).bindFromRequest(request);
		Tontine tontine = form.get();
		
		if(tontineDAO.update(tontine).equals("ok"))
			System.out.println("Modification effectuer avec success");
		else
			System.err.println("Modification non effectuer" + tontineDAO.update(tontine));
		
		
		return ok(views.html.AfficherTontine.render(tontineDAO.listeT()));

	}
	
	public Result deleteT(Request request, int id) throws ClassNotFoundException, SQLException{
		tontineDAO.suppression(id);
		return ok(views.html.AfficherTontine.render(tontineDAO.listeT()));

	}

}
