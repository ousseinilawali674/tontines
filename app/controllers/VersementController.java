package controllers;

import java.sql.SQLException;
import javax.inject.Inject;
import play.data.Form;
import play.mvc.*;
import play.mvc.Http.Request;
import tontine.InscriptionDAO;
import tontine.VersementDAO;
import play.data.FormFactory;
public class VersementController extends Controller{
	FormFactory dataForm;
	VersementDAO versementAO;
	@Inject
	
	public VersementController(FormFactory dataForm, VersementDAO versementAO) {
		super();
		this.dataForm = dataForm;
		this.versementAO = versementAO;
	}
	
	public Result createV(Request request){
		return ok(views.html.CreateV.render(request));
	}
}
