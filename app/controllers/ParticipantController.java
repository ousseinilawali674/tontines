package controllers;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;


import play.data.Form;
import play.data.FormFactory;
import play.mvc.*;
import play.mvc.Http.Request;
import tontine.Participant;
import tontine.ParticipantDAO;

public class ParticipantController extends Controller {

	FormFactory dataForm;
	ParticipantDAO participantDAO;
	@Inject
	
	public ParticipantController(FormFactory dataForm, ParticipantDAO participantDAO) {
		super();
		this.dataForm = dataForm;
		this.participantDAO = participantDAO;
	}
	
	public Result createP(Request request){
		return ok(views.html.CreateP.render(request));
	}
	
	public Result saveP(Request request) throws ClassNotFoundException, SQLException {

		Form<Participant> form = dataForm.form(Participant.class).bindFromRequest(request);
		Participant participant = form.get();
				
		
//		String p = dataForm.form().bindFromRequest(request).get("dateNaissance");
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
//		
//		Date d1 = sdf.parse("01-01-2002");
//		Date d = sdf.parse(p);
//		if( d.before(d1) ){
			if(participantDAO.ajouter(participant).equals("ok"))
				System.out.println("insertion effectuer avec success");
			else
				System.err.println("insertion non effectuer" + participantDAO.ajouter(participant));
			
			return ok(views.html.AfficherParticipant.render(participantDAO.listeP()));
//		}
//		else{
//			return ok(views.html.CreateP.render(request));
//		}

	}
	
	public Result listeP(Request request) throws ClassNotFoundException, SQLException{
		return ok(views.html.AfficherParticipant.render(participantDAO.listeP()));
	}

	public Result modifView(Request request, int id) throws ClassNotFoundException, SQLException{
		return ok(views.html.updateP.render(participantDAO.findByID(id),request));
	}
	
	public Result updateP(Request request) throws ClassNotFoundException, SQLException{
		Form<Participant> form = dataForm.form(Participant.class).bindFromRequest(request);
		Participant participant = form.get();
		
		if(participantDAO.update(participant).equals("ok"))
			System.out.println("Modification effectuer avec success");
		else
			System.err.println("Modification non effectuer" + participantDAO.update(participant));
		
		
		return ok(views.html.AfficherParticipant.render(participantDAO.listeP()));

	}
	
	public Result deleteP(Request request, int id) throws ClassNotFoundException, SQLException{
		participantDAO.suppression(id);
		return ok(views.html.AfficherParticipant.render(participantDAO.listeP()));

	}

	
}
