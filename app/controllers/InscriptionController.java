package controllers;

import java.sql.SQLException;
import javax.inject.Inject;
import play.data.Form;
import play.mvc.*;
import play.mvc.Http.Request;
import tontine.InscriptionDAO;
import tontine.ParticipantDAO;
import play.data.FormFactory;

public class InscriptionController extends Controller{
	FormFactory dataForm;
	InscriptionDAO inscriptionAO;
	@Inject
	
	public InscriptionController(FormFactory dataForm, InscriptionDAO inscriptionAO) {
		super();
		this.dataForm = dataForm;
		this.inscriptionAO = inscriptionAO;
	}
	
	public Result createI(Request request){
		return ok(views.html.CreateI.render(request));
	}
}
