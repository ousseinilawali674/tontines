name := """Newproject"""
organization := "comiai"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.13.3"

libraryDependencies += guice

libraryDependencies ++= Seq( "mysql" % "mysql-connector-java" % "5.1.24")
libraryDependencies ++= Seq(
  javaJdbc
)
